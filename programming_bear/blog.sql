--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: post; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE post (
    id integer NOT NULL,
    title character varying(144),
    post_date date,
    content text
);


ALTER TABLE public.post OWNER TO postgres;

--
-- Data for Name: post; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY post (id, title, post_date, content) FROM stdin;
1	<p>My name is Peter Rauhut and this is my site. I have been studying programming for a little over a decade, but I have been extremely lax on myself about actually picking up projects. I started studying C++ in high school, around 2001-2002 and learned the basics of HTML around the same time. I have taken college courses in C++, C#, Java, VB.NET, Web Development using HTML/CSS and Python as well as Unix/Linux and a variety of other topics. I am currently still pursuing my education in my spare time, but that is slow going, so I decided to work on projects aside from that, as well. </p>\n    <p>This blog is being setup as an attempt to force myself to work on projects. I'm interested in a great many things, so I'm not always sitting down working on something related to programming. I am interested in pursuing a career in development and am working on side projects to help me achieve that goal. I have about 5 years of work experience in QA as well as a number of years in customer support for end users and IT level support for large scale corportations. The last 6 years have all involved automation to some degree, including work with JMeter, Selenium and Locust.io. I have also been involved in writing automated deployment scripts including time supporting a cloud management platform that uses Bash and Javascripting in conjunction with Puppet to deploy instances into AWS, VSphere, Openstack and other environments.</p>\n    <p>On a more personal note my life took a bit of a turn in 2012 when I met the most amazing person. In 2014, she agreed to marry me and our wedding was October of 2015. We had an amazing honeymoon where we visited many of the things that Iceland has to offer including a scuba trip in the Silfra fissure. I have lived in Austin, TX my whole life and continue to live here with my wife and our two dogs. In my spare time, I practice Shaolin Kung-Fu and Yoga as well as play video games, watch movies/TV on Netflix/Hulu/HBO Now, and read in addition to work on my individual projects I'm hoping to display here.</p>\n    <p>I have named this site Programming-Bear.com. My wife took to calling me Bear early in our relationship, as a term of endearment, so I started to use it in my daily life in many ways, such as character names and here.</p>
\.


--
-- Name: post_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY post
    ADD CONSTRAINT post_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: post; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE post FROM PUBLIC;
REVOKE ALL ON TABLE post FROM postgres;
GRANT ALL ON TABLE post TO postgres;
GRANT SELECT ON TABLE post TO readblog;
GRANT SELECT,INSERT,UPDATE ON TABLE post TO writeblog;


--
-- PostgreSQL database dump complete
--

