from flask import Flask, abort
from flask import render_template
from datetime import datetime, date
import db_worker
import markdown

app = Flask(__name__)


@app.route('/')
@app.route('/blog')
@app.route('/blog/<url_date>')
def blog(url_date=None):
    db = db_worker.DB_Worker()
    if url_date:
        try:
            udate = datetime.strptime(url_date, '%Y%m%d')
        except:
            abort(404)
        post = db.collect_post(date=udate)
    else:
        post = db.collect_post()
    if post is None:
        abort(404)
    post_title = post[0]
    post_content = markdown.markdown(post[1], extensions=[
                                     'markdown.extensions.attr_list',
                                     'markdown.extensions.fenced_code'])
    post_id = post[2]
    post_date = post[3]
    prev_date = date.strftime(post[4], '%Y%m%d') if post[4] else None
    next_date = date.strftime(post[5], '%Y%m%d') if post[5] else None
    return render_template('blog',
                           title=post_title,
                           body=post_content,
                           date=post_date,
                           id=post_id,
                           prev_date=prev_date,
                           next_date=next_date)


@app.route('/aboutme')
def about_me():
    db = db_worker.DB_Worker()
    post = db.collect_aboutme()
    post_title = post[0]
    post_body = post[1]
    return render_template('aboutme', title=post_title, body=post_body)


@app.route('/resume')
def resume():
    post_title = 'Resume'
    return render_template('resume', title=post_title)


@app.route('/portfolio')
def portfolio():
    post_title = 'Portfolio'
    return render_template('portfolio', title=post_title)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('page_not_found'), 404


if __name__ == '__main__':
    app.debug = True
    app.run('0.0.0.0')
