import psycopg2


class DB_Worker:

    def __init__(self):
        self.database = psycopg2.connect(
            "dbname=%s user=%s password=%s host=%s" % (
                "blog",
                "readblog",
                "devpassword",
                "localhost"
            )
        )

    def collect_post(self, date=0):
        cur = self.database.cursor()
        if date == 0:
            cur.execute("SELECT title,\
                        content,\
                        id,\
                        post_date,\
                        prev_date,\
                        next_date\
                        FROM Post\
                        ORDER BY id DESC LIMIT 1;")
        else:
            cur.execute("SELECT title,\
                        content,\
                        id,\
                        post_date,\
                        prev_date,\
                        next_date\
                        FROM Post\
                        WHERE post_date = '%s';" % date)
        post = cur.fetchone()
        if post is not None:
            return post
        else:
            return None

    def collect_aboutme(self):
        cur = self.database.cursor()

        cur.execute("SELECT title, content FROM Post        \
            WHERE title = '%s';" % "About Me")
        return cur.fetchone()

    def collect_resume(self):
        cur = self.database.cursor()

        cur.execute("SELECT title, content FROM Post        \
            WHERE title = '%s';" % "Resume")
        return cur.fetchone()

    def collect_portfolio(self):
        cur = self.database.cursor()

        cur.execute("SELECT title, content FROM Post        \
            WHERE title = '%s';" % "Portfolio")
        return cur.fetchone()

    def add_post(self, date, title, content):
        cur = self.database.cursor()

        cur.execute("SELECT max(id) FROM POST;")
        post_id = cur.fetchone()[0]
        if post_id is None:
            post_id = 1
        else:
            post_id += 1

        cur.execute("INSERT INTO Post (                     \
                id,                                         \
                post_date,                                  \
                title,                                      \
                content                                     \
                ) VALUES (                                  \
                %s,                                         \
                %s,                                         \
                %s,                                         \
                %s                                          \
                );",
                    (
                        post_id,
                        date,
                        title,
                        content
                    ))
        self.database.commit()
        cur.close()

    def update_post(self, date, title, content):
        cur = self.database.cursor()

        cur.execute("SELECT id FROM Post                    \
            WHERE post_date = '%s';" % date)

        if cur.fetchone():
            cur.execute("UPDATE Post SET                    \
                post_date   = '%s',                         \
                title       = '%s',                         \
                content     = '%s'                          \
                WHERE post_date = '%s';" %
                        (
                            date,
                            title,
                            content,
                            date
                        ))
            self.database.commit()
            cur.close()
            return 200
        else:
            cur.close()
            return 404
